group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.71"
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("modbus") {
                val modbusDir = "${System.getProperty("user.home")}/libmodbus-3.1.6"
                includeDirs("$modbusDir/include/modbus")
                // TODO: Find a way to set the library path for the linker in this build file instead of hard coding
                //  the absolute path in the modbus.def file.
                linkerOpts("-L$modbusDir/lib", "-lmodbus")
            }
        }
        binaries {
            executable("modbus_client") {
                entryPoint = "org.example.modbus.client.main"
            }
        }
    }
}