@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.modbus.client

import kotlinx.cinterop.*
import modbus.*
import platform.posix.*
import kotlin.system.exitProcess

private const val DEBUG_ENABLED = true
private const val READ_INPUT_REGISTER = 4
private const val WRITE_HOLDING_REGISTER = 6

fun main(args: Array<String>) = memScoped {
    checkProgramArguments(args)
    val host = args.last().split(":").first()
    val port = args.last().split(":").last().toInt()
    println("Starting Modbus client...")
    val modbusCtx = createModbusCtx(slaveId = args[3].toInt(), host = host, port = port)
    println("Connecting to Modbus server (on $host:$port)...")
    connectToServer(modbusCtx)

    if (args.first().toInt() == WRITE_HOLDING_REGISTER) {
        val registerAddr = args[1].toInt()
        val registerValue = args[2].toUShort()
        writeHoldingRegister(modbusCtx, registerAddr = registerAddr, value = registerValue)
    } else {
        val registerAddr = args[1].toInt()
        val registerLength = args[2].toInt()
        val values = readInputRegisters(modbusCtx = modbusCtx, registerLength = registerLength,
            registerAddr = registerAddr)
        values.forEach { println("* Value: $it") }
    }
    println("Exiting...")
    cleanUp(modbusCtx)
    Unit
}

private fun createModbusCtx(host: String, port: Int, slaveId: Int): CPointer<modbus_t>? {
    val result = modbus_new_tcp(host, port)
    modbus_set_debug(result, DEBUG_ENABLED.intValue)
    modbus_set_response_timeout(ctx = result, to_sec = 6u, to_usec = 0u)
    if (slaveId > 0) modbus_set_slave(result, slaveId)
    return result
}

private fun checkProgramArguments(args: Array<String>) {
    val functionTypes = arrayOf(READ_INPUT_REGISTER, WRITE_HOLDING_REGISTER)
    val requiredArgs = 5
    val valid = when {
        args.size != requiredArgs -> false
        ":" !in args.last() -> false
        !args.last().split(":").last().containsInt() -> false
        !args.first().containsInt() -> false
        args.first().toInt() !in functionTypes -> false
        args.first().toInt() == READ_INPUT_REGISTER -> validateReadInputRegisterArgs(args)
        args.first().toInt() == WRITE_HOLDING_REGISTER -> validateWriteHoldingRegisterArgs(args)
        else -> true
    }
    if (!valid) {
        printProgramUsage()
        exitProcess(-1)
    }
}

@Suppress("RedundantIf")
private fun validateReadInputRegisterArgs(args: Array<String>) =
    if (!args[1].containsInt()) false
    else if (!args[2].containsInt()) false
    else if (!args[3].containsInt()) false
    else true

@Suppress("RedundantIf")
private fun validateWriteHoldingRegisterArgs(args: Array<String>) =
    if (!args[1].containsInt()) false
    else if (!args[2].containsInt()) false
    else if (!args[3].containsInt()) false
    else true

private fun String.containsInt() = try {
    toInt()
    true
} catch (ex: NumberFormatException) {
    false
}

private fun printProgramUsage() {
    println("""
        ---- Modbus Client Usage ----
        modbus_client 4 <registerAddr> <registerLength> <slaveId> <host>:<port> (eg modbus_client 4 1 2 126 192.168.1.1:502)
        modbus_client 6 <registerAddr> <value> <slaveId> <host>:<port> (eg modbus_client 6 1 7 126 192.168.1.1:502)
    """.trimIndent())
}

private fun readInputRegisters(modbusCtx: CPointer<modbus_t>?, registerAddr: Int, registerLength: Int) = memScoped {
    val tmp = mutableListOf<UShort>()
    val registers = allocArray<uint16_tVar>(registerLength)
    val readRegisters = modbus_read_input_registers(ctx = modbusCtx, dest = registers, addr = registerAddr,
        nb = registerLength)
    if (readRegisters != registerLength) fprintf(stderr, "Failed to read: %s\n", modbus_strerror(errno))
    @Suppress("ReplaceRangeToWithUntil")
    (0..(registerLength - 1)).forEach { tmp += registers[it] }
    tmp.toTypedArray()
}

private fun writeHoldingRegister(modbusCtx: CPointer<modbus_t>?, registerAddr: Int, value: UShort) {
    val errorCode = -1
    if (modbus_write_register(modbusCtx, registerAddr, value) == errorCode) {
        fprintf(stderr, "Failed to write: %s\n", modbus_strerror(errno))
    }
}

private fun connectToServer(modbusCtx: CPointer<modbus_t>?) {
    val errorCode = -1
    if (modbus_connect(modbusCtx) == errorCode) {
        fprintf(stderr, "Failed to connect: %s\n", modbus_strerror(errno))
        println("Exiting...")
        cleanUp(modbusCtx)
        exitProcess(errorCode)
    }
}

private fun cleanUp(modbusCtx: CPointer<modbus_t>?) {
    modbus_close(modbusCtx)
    modbus_free(modbusCtx)
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0
